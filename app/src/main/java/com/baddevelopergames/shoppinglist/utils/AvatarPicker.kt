package com.baddevelopergames.shoppinglist.utils

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.widget.ImageView
import com.baddevelopergames.shoppinglist.R
import io.reactivex.Observable

// todo refactor this crap
class AvatarPicker constructor(private val context: Context) {
    fun pick(): Observable<String> {
        val customView = LayoutInflater.from(context).inflate(R.layout.item_avatar_picker, null)
        val img1 = customView.findViewById<ImageView>(R.id.avatar_picker_1)
        val img2 = customView.findViewById<ImageView>(R.id.avatar_picker_2)
        val img3 = customView.findViewById<ImageView>(R.id.avatar_picker_3)
        val img4 = customView.findViewById<ImageView>(R.id.avatar_picker_4)
        val img5 = customView.findViewById<ImageView>(R.id.avatar_picker_5)
        val img6 = customView.findViewById<ImageView>(R.id.avatar_picker_6)
        val img7 = customView.findViewById<ImageView>(R.id.avatar_picker_7)
        val img8 = customView.findViewById<ImageView>(R.id.avatar_picker_8)
        val img9 = customView.findViewById<ImageView>(R.id.avatar_picker_9)
        val img10 = customView.findViewById<ImageView>(R.id.avatar_picker_10)
        val img11 = customView.findViewById<ImageView>(R.id.avatar_picker_11)
        val img12 = customView.findViewById<ImageView>(R.id.avatar_picker_12)

        return Observable.create { emitter ->
            val dialog = AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.pick_avatar))
                    .setView(customView)
                    .create()
            dialog.show()
            img1.setOnClickListener {
                emitter.onNext("boy")
                dialog.dismiss()
            }
            img2.setOnClickListener {
                emitter.onNext("girl")
                dialog.dismiss()
            }
            img3.setOnClickListener {
                emitter.onNext("grandparents")
                dialog.dismiss()
            }
            img4.setOnClickListener {
                emitter.onNext("task")
                dialog.dismiss()
            }
            img5.setOnClickListener {
                emitter.onNext("farmer")
                dialog.dismiss()
            }
            img6.setOnClickListener {
                emitter.onNext("hands")
                dialog.dismiss()
            }
            img7.setOnClickListener {
                emitter.onNext("hippie")
                dialog.dismiss()
            }
            img8.setOnClickListener {
                emitter.onNext("loop")
                dialog.dismiss()
            }
            img9.setOnClickListener {
                emitter.onNext("moneybag")
                dialog.dismiss()
            }
            img10.setOnClickListener {
                emitter.onNext("nerd")
                dialog.dismiss()
            }
            img11.setOnClickListener {
                emitter.onNext("profesor")
                dialog.dismiss()
            }
            img12.setOnClickListener {
                emitter.onNext("safebox")
                dialog.dismiss()
            }
        }
    }

    fun get(name: String?): Int {
        return when (name) {
            "boy" -> R.drawable.boy
            "girl" -> R.drawable.girl
            "grandparents" -> R.drawable.grandparents
            "task" -> R.drawable.task
            "farmer" -> R.drawable.farmer
            "hands" -> R.drawable.hands
            "hippie" -> R.drawable.hippie
            "loop" -> R.drawable.loop
            "moneybag" -> R.drawable.moneybag
            "nerd" -> R.drawable.nerd
            "profesor" -> R.drawable.profesor
            "safebox" -> R.drawable.safebox
            else -> R.drawable.boy
        }
    }
}
