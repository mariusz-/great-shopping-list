package com.baddevelopergames.shoppinglist.utils

import com.baddevelopergames.shoppinglist.db.BaseFirebaseModel

fun <E : BaseFirebaseModel> List<E>.sortAlphabetically(): List<E> {
    return this.sortedWith(compareBy { it.name!!.toLowerCase() })
}
