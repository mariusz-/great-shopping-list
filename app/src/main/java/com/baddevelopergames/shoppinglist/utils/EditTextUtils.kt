package com.baddevelopergames.shoppinglist.utils

import android.content.Context
import android.view.inputmethod.InputMethodManager
import android.widget.EditText

fun EditText.showKeyboard(context: Context) {
    this.requestFocus()
    val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.toggleSoftInputFromWindow(this.applicationWindowToken,
            InputMethodManager.SHOW_FORCED, 0)
}
