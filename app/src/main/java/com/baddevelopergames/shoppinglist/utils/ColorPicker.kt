package com.baddevelopergames.shoppinglist.utils

import android.content.Context
import com.baddevelopergames.shoppinglist.R
import com.flask.colorpicker.ColorPickerView
import com.flask.colorpicker.builder.ColorPickerDialogBuilder
import io.reactivex.Observable

class ColorPicker constructor(private val context: Context) {
    fun pick(initialColor: Int): Observable<Int> {
        return Observable.create { emitter ->
            ColorPickerDialogBuilder
                    .with(context)
                    .setTitle(context.getString(R.string.choose_color))
                    .initialColor(initialColor)
                    .wheelType(ColorPickerView.WHEEL_TYPE.FLOWER)
                    .density(12)
                    .setPositiveButton(context.getString(R.string.ok)) { _, p1, _ -> emitter.onNext(p1) }
                    .build()
                    .show()
        }
    }

    fun name(color: Int) = String.format("#%06X", 0xFFFFFF and color)
}
