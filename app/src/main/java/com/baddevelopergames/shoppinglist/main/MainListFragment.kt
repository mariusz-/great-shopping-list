package com.baddevelopergames.shoppinglist.main

import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.baddevelopergames.shoppinglist.R
import com.baddevelopergames.shoppinglist.base.RxFragment
import com.baddevelopergames.shoppinglist.db.DbExecutor
import com.baddevelopergames.shoppinglist.db.GroceryModel
import com.baddevelopergames.shoppinglist.db.ListModel
import kotlinx.android.synthetic.main.fragment_main_list.*

class MainListFragment : RxFragment(), MainListFragmentView,
    MainListAdapter.MainListAdapterInteractions {
    override val presenter by lazy {
        val dbExecutor = DbExecutor()
        MainListFragmentPresenter(this, dbExecutor)
    }
    private var adapter: MainListAdapter? = null
    private val listId: String by lazy {
        val bundle = arguments
        bundle!!.getString(TAG_LIST_ID, "")
    }
    private val email: String by lazy {
        val bundle = arguments
        bundle!!.getString(TAG_LIST_EMAIL, "")
    }

    companion object {
        private const val TAG_LIST_COLOR = "TAG_LIST_COLOR"
        private const val TAG_LIST_ID = "TAG_LIST_ID"
        private const val TAG_LIST_EMAIL = "TAG_LIST_EMAIL"

        fun newInstance(listModel: ListModel): Fragment {
            val bundle = Bundle()
            bundle.putString(TAG_LIST_COLOR, listModel.color)
            bundle.putString(TAG_LIST_ID, listModel.uid)
            bundle.putString(TAG_LIST_EMAIL, listModel.email)
            val mainListFragment = MainListFragment()
            mainListFragment.arguments = bundle
            return mainListFragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main_list, container, false)
    }

    override fun onResume() {
        super.onResume()
        val bundle = arguments!!
        val color = bundle.getString(TAG_LIST_COLOR, "")
        content.setBackgroundColor(Color.parseColor(color))
        presenter.init()
        presenter.fetchData(listId, email)
    }

    override fun onRefresh(groceryModel: GroceryModel) {
        presenter.refreshGrocery(groceryModel, listId)
    }

    override fun onRemove(groceryModel: GroceryModel) {
        presenter.removeGrocery(groceryModel, listId, email)
    }

    override fun onUrlClicked(groceryModel: GroceryModel) {
        presenter.onUrlClicked(groceryModel)
    }

    override fun initRecyclerView(list: List<GroceryModel>) {
        val lm = LinearLayoutManager(context)
        recycler_view.layoutManager = lm
        adapter = MainListAdapter(list, this)
        recycler_view.setHasFixedSize(true)
        recycler_view.adapter = adapter
    }

    override fun showMessage(message: String?) {
        context?.let { Toast.makeText(context, message, Toast.LENGTH_SHORT).show() }
    }

    override fun notifyDataSetChanged() {
        adapter?.notifyDataSetChanged()
    }

    override fun email() = email

    override fun startUrlIntent(intent: Intent) {
        try {
            startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            showMessage(getString(R.string.url_opening_failure))
        }
    }

    override fun getSort(): Sorting = MainActivity.getSort(requireContext())
}
