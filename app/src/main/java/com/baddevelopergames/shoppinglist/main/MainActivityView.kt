package com.baddevelopergames.shoppinglist.main

import com.baddevelopergames.shoppinglist.db.ListModel

interface MainActivityView {
    fun openUserListActivity()

    fun initRecyclerView(list: List<ListModel>)

    fun initViewPager(list: List<ListModel>)

    fun notifyDataSetChanged()

    fun showMessage(message: String?)

    fun showMessage(message: Int)

    fun setPagerPosition(position: Int)

    fun getPagerPosition(): Int

    fun getAddGroceryName(): String

    fun enableBottomPanel(enable: Boolean)

    fun clearBottomPanel()

    fun refreshPagerItem(position: Int)

    fun openAddListActivity()

    fun showViewPager(visibility: Int)

    fun setFocus()

    fun initBottomPanelHint(name: String)

    fun upperPanelVisibility(visibility: Int)

    fun getUpperPanelVisibility(): Int

    fun getUrlText(): String
}