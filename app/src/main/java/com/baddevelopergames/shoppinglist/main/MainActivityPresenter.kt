package com.baddevelopergames.shoppinglist.main

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.baddevelopergames.shoppinglist.R
import com.baddevelopergames.shoppinglist.base.RxPresenter
import com.baddevelopergames.shoppinglist.db.DbExecutor
import com.baddevelopergames.shoppinglist.db.GroceryModel
import com.baddevelopergames.shoppinglist.db.ListModel
import com.baddevelopergames.shoppinglist.db.UserInfo
import com.baddevelopergames.shoppinglist.utils.sortAlphabetically
import java.util.*

class MainActivityPresenter constructor(
    private val view: MainActivityView,
    private val dbExecutor: DbExecutor,
    private val userInfo: UserInfo
) : RxPresenter() {
    private val list = ArrayList<ListModel>()
    private var redirected = false

    companion object {
        const val TAG_REDIRECTED = "TAG_REDIRECTED"
    }

    fun init(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            redirected = savedInstanceState.getBoolean(TAG_REDIRECTED)
        }
        view.initRecyclerView(list)
        view.enableBottomPanel(false)
        view.initBottomPanelHint(userInfo.name())
    }

    fun onAddGroceryClicked() {
        val currentPos = view.getPagerPosition()
        val parentId = list[currentPos].uid
        val name = view.getAddGroceryName()
        val email = list[currentPos].email!!
        val urlVisible = view.getUpperPanelVisibility()
        val url = if (urlVisible == View.VISIBLE) {
            view.getUrlText()
        } else {
            null
        }
        if (TextUtils.isEmpty(name)) {
            view.showMessage(R.string.cannot_be_empty)
        } else {
            addGrocery(name, parentId, currentPos, email, url)
        }
    }

    private fun addGrocery(
        name: String,
        parentId: String,
        currentPos: Int,
        email: String,
        url: String?
    ) {
        view.enableBottomPanel(false)
        rxRequest(
            dbExecutor.addGrocery(name.trim(), parentId, email.trim(), url?.trim()?.addHttp(), 0)
                .subscribe(
                    {
                        view.clearBottomPanel()
                        view.enableBottomPanel(true)
                        view.setFocus()
                        view.refreshPagerItem(currentPos)
                    },
                    {
                        view.showMessage(it.message)
                        view.enableBottomPanel(true)
                    }
                ))
    }

    fun fetchLists() {
        rxRequest(
                dbExecutor.getList().subscribe(
                        {
                            list.clear()
                            list.addAll(it.sortAlphabetically())
                            view.notifyDataSetChanged()
                            if (list.isEmpty()) {
                                view.showViewPager(View.GONE)
                                if (redirected) {
                                    view.enableBottomPanel(false)
                                } else {
                                    view.openAddListActivity()
                                    redirected = true
                                }
                            } else {
                                view.initViewPager(list)
                                view.enableBottomPanel(true)
                                view.showViewPager(View.VISIBLE)
                            }
                        },
                        {
                            view.showMessage(it.message)
                        }
                ))
    }

    fun onSaveInstanceState(outState: Bundle?) {
        outState?.putBoolean(TAG_REDIRECTED, redirected)
    }

    fun onTitleClick(position: Int) {
        view.setPagerPosition(position)
    }

    fun onAddListClicked() {
        view.openAddListActivity()
    }

    fun onAddLinkClicked() {
        view.upperPanelVisibility(View.VISIBLE)
    }

    fun onHideLinkClicked() {
        view.upperPanelVisibility(View.GONE)
    }
}

fun String.addHttp(): String {
    return if (!this.startsWith("http")) {
        "http://${this}"
    } else {
        this
    }
}
