package com.baddevelopergames.shoppinglist.main

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.baddevelopergames.shoppinglist.R
import com.baddevelopergames.shoppinglist.db.GroceryModel
import kotlinx.android.synthetic.main.row_grocery.view.*

class MainListAdapter constructor(private val list: List<GroceryModel>,
                                  private val interactions: MainListAdapterInteractions)
    : RecyclerView.Adapter<MainListAdapter.MainListViewHolder>() {
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainListViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context)
                .inflate(R.layout.row_grocery, parent, false)
        return MainListViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: MainListViewHolder, position: Int) {
        val groceryModel = list[position]
        holder.bind(context, groceryModel, interactions)
    }

    class MainListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(context: Context, groceryModel: GroceryModel, interactions: MainListAdapterInteractions) {
            itemView.grocery.setText(groceryModel.name)

            itemView.refresh_item.setOnClickListener {
                val newName = itemView.grocery.text.toString()
                if (TextUtils.isEmpty(newName)) {
                    Toast.makeText(context, R.string.cannot_be_empty, Toast.LENGTH_SHORT).show()
                    itemView.grocery.setText(groceryModel.name)
                } else {
                    groceryModel.name = newName
                    interactions.onRefresh(groceryModel)
                }
            }

            itemView.delete_from_list.setOnClickListener {
                interactions.onRemove(groceryModel)
            }

            itemView.item_url.visibility = if (groceryModel.url == null
                    || groceryModel.url == "http://" || groceryModel.url == "https://") {
                View.GONE
            } else {
                itemView.item_url.setOnClickListener {
                    interactions.onUrlClicked(groceryModel)
                }
                View.VISIBLE
            }
            if (groceryModel.shop == 1) {
                itemView.shop.visibility = View.VISIBLE
                itemView.shop.setImageResource(R.drawable.ic_biedronka)
            } else if (groceryModel.shop == 2) {
                itemView.shop.visibility = View.VISIBLE
                itemView.shop.setImageResource(R.drawable.ic_rossman)
            } else {
                itemView.shop.visibility = View.GONE
            }
        }
    }

    interface MainListAdapterInteractions {
        fun onRefresh(groceryModel: GroceryModel)

        fun onRemove(groceryModel: GroceryModel)

        fun onUrlClicked(groceryModel: GroceryModel)
    }
}
