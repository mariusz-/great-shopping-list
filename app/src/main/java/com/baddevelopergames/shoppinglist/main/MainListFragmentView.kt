package com.baddevelopergames.shoppinglist.main

import android.content.Intent
import com.baddevelopergames.shoppinglist.db.GroceryModel

interface MainListFragmentView {
    fun initRecyclerView(list: List<GroceryModel>)

    fun showMessage(message: String?)

    fun notifyDataSetChanged()

    fun email(): String

    fun startUrlIntent(intent: Intent)

    fun getSort(): Sorting
}