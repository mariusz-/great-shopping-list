package com.baddevelopergames.shoppinglist.main

enum class Sorting {
    ALPHABETICAL, TIMESTAMP
}