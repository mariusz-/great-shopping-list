package com.baddevelopergames.shoppinglist.main

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.baddevelopergames.shoppinglist.db.ListModel

class MainListPagerAdapter constructor(fragmentManager: FragmentManager,
                                       private val list: List<ListModel>)
    : FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        val itemContent = list[position]
        return MainListFragment.newInstance(itemContent)
    }

    override fun getCount() = list.size
}
