package com.baddevelopergames.shoppinglist.main

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.baddevelopergames.shoppinglist.R
import com.baddevelopergames.shoppinglist.base.RxActivity
import com.baddevelopergames.shoppinglist.db.DbExecutor
import com.baddevelopergames.shoppinglist.db.ListModel
import com.baddevelopergames.shoppinglist.db.UserInfo
import com.baddevelopergames.shoppinglist.settings.addlist.AddListActivity
import com.baddevelopergames.shoppinglist.settings.userlist.UserListActivity
import com.baddevelopergames.shoppinglist.utils.AvatarPicker
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : RxActivity(), MainActivityView,
    MainTitleListAdapter.MainTitleListAdapterInteractions {
    private var titleListAdapter: MainTitleListAdapter? = null
    override val presenter by lazy {
        val dbExecutor = DbExecutor()
        val userInfo = UserInfo()
        MainActivityPresenter(this, dbExecutor, userInfo)
    }
    private var adapter: MainListPagerAdapter? = null

    companion object {
        private const val TAG_TEXT = "TAG_TEXT"

        fun callingIntent(context: Context, text: String? = null): Intent {
            return Intent(context, MainActivity::class.java).apply {
                putExtra(TAG_TEXT, text)
            }
        }

        fun getSort(context: Context): Sorting {
            val sharedPreferences: SharedPreferences =
                context.getSharedPreferences("my.app.packagename_preferences", Context.MODE_PRIVATE)
            val savedSort: String = sharedPreferences.getString("SORT", Sorting.ALPHABETICAL.name)
                ?: Sorting.ALPHABETICAL.name
            return Sorting.valueOf(savedSort)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)

        presenter.init(savedInstanceState)
        add_input_iv.setOnClickListener { presenter.onAddGroceryClicked() }
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        add_list.setOnClickListener { presenter.onAddListClicked() }
        add_link_iv.setOnClickListener { presenter.onAddLinkClicked() }
        hide_link_iv.setOnClickListener { presenter.onHideLinkClicked() }

        fetchDeeplink()
    }

    override fun onResume() {
        super.onResume()
        presenter.fetchLists()
        input_et.requestFocus()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.settings) {
            startActivity(UserListActivity.callingIntent(this))
        }
        if (item?.itemId == R.id.sort_alphabetical) {
            sort(Sorting.ALPHABETICAL)
        }
        if (item?.itemId == R.id.sort_timestamp) {
            sort(Sorting.TIMESTAMP)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun openUserListActivity() {
        startActivity(UserListActivity.callingIntent(this))
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        presenter.onSaveInstanceState(outState)
    }

    override fun initRecyclerView(list: List<ListModel>) {
        val lm = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        title_rv.layoutManager = lm
        val avatarPicker = AvatarPicker(this)
        titleListAdapter = MainTitleListAdapter(list, avatarPicker, windowManager, this)
        title_rv.setHasFixedSize(true)
        title_rv.adapter = titleListAdapter
    }

    override fun initViewPager(list: List<ListModel>) {
        adapter = MainListPagerAdapter(supportFragmentManager, list)
        content_vp.adapter = adapter
    }

    override fun notifyDataSetChanged() {
        titleListAdapter?.notifyDataSetChanged()
        adapter?.notifyDataSetChanged()
    }

    override fun showMessage(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showMessage(message: Int) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onTitleClick(position: Int) {
        presenter.onTitleClick(position)
    }

    override fun setPagerPosition(position: Int) {
        content_vp.currentItem = position
    }

    override fun getPagerPosition(): Int = content_vp.currentItem

    override fun getAddGroceryName(): String = input_et.text.toString()

    override fun enableBottomPanel(enable: Boolean) {
        input_et.isFocusableInTouchMode = enable
        input_et.isFocusable = enable
        add_input_iv.isEnabled = enable
        when (enable) {
            true -> bottom_panel.alpha = 1.0f
            false -> bottom_panel.alpha = 0.5f
        }

        input_link_et.isFocusableInTouchMode = enable
        input_link_et.isFocusable = enable
        input_link_et.isEnabled = enable
        when (enable) {
            true -> upper_panel.alpha = 1.0f
            false -> upper_panel.alpha = 0.5f
        }
    }

    override fun clearBottomPanel() {
        input_et.setText("")
        input_link_et.setText("")
    }

    override fun refreshPagerItem(position: Int) {
        val currentListFragment: Fragment =
            content_vp.adapter?.instantiateItem(content_vp, position) as Fragment
        currentListFragment.onResume()
    }

    override fun openAddListActivity() {
        startActivity(AddListActivity.callingIntent(this))
    }

    override fun showViewPager(visibility: Int) {
        content_vp.visibility = visibility
    }

    override fun setFocus() {
        input_et.requestFocus()
    }

    override fun initBottomPanelHint(name: String) {
        input_et.hint = getString(R.string.bottom_panel_hint, name)
    }

    override fun upperPanelVisibility(visibility: Int) {
        upper_panel.visibility = visibility
        if (visibility == View.GONE) {
            input_link_et.setText("")
        }
    }

    override fun getUpperPanelVisibility(): Int = upper_panel.visibility

    override fun getUrlText(): String = input_link_et.text.toString()

    private fun fetchDeeplink() {
        intent.getStringExtra(TAG_TEXT)?.let {
            if (it.startsWith("http://") || it.startsWith("https://")) {
                presenter.onAddLinkClicked()
                input_link_et.setText(it)
            } else {
                input_et.setText(it)
            }
        }
    }

    private fun sort(type: Sorting) {
        saveSort(type)
        presenter.fetchLists()
    }

    private fun saveSort(sort: Sorting) {
        val sharedPreferences: SharedPreferences =
            getSharedPreferences("my.app.packagename_preferences", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("SORT", sort.name)
        editor.apply()
    }
}
