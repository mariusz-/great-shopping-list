package com.baddevelopergames.shoppinglist.main

import android.content.Intent
import android.net.Uri
import com.baddevelopergames.shoppinglist.base.RxPresenter
import com.baddevelopergames.shoppinglist.db.DbExecutor
import com.baddevelopergames.shoppinglist.db.GroceryModel
import com.baddevelopergames.shoppinglist.utils.sortAlphabetically

class MainListFragmentPresenter constructor(
    private val view: MainListFragmentView,
    private val dbExecutor: DbExecutor
) : RxPresenter() {
    private val groceryList = ArrayList<GroceryModel>()

    fun init() {
        view.initRecyclerView(groceryList)
    }

    fun fetchData(listId: String, email: String) {
        rxRequest(
            dbExecutor.getGroceryList(listId, email)
                .subscribe(
                    { response ->
                        groceryList.clear()
                        when (view.getSort()) {
                            Sorting.ALPHABETICAL -> {
                                groceryList.addAll(response.sortAlphabetically())
                            }
                            Sorting.TIMESTAMP -> {
                                groceryList.addAll(
                                    response.sortedByDescending {
                                        it.uid.toLong()
                                    }
                                )
                            }
                        }
                        view.notifyDataSetChanged()
                    },
                    {
                        view.showMessage(it.message)
                    })
        )
    }

    fun removeGrocery(groceryModel: GroceryModel, listId: String, email: String) {
        rxRequest(
            dbExecutor.removeGrocery(groceryModel.uid, listId, email)
                .onErrorReturn { view.showMessage(it.message) }
                .subscribe())
    }

    fun refreshGrocery(groceryModel: GroceryModel, listId: String) {
        rxRequest(
            dbExecutor.refreshGrocery(groceryModel, listId, view.email())
                .onErrorReturn { view.showMessage(it.message) }
                .subscribe())
    }

    fun onUrlClicked(groceryModel: GroceryModel) {
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(groceryModel.url)
        view.startUrlIntent(i)
    }
}
