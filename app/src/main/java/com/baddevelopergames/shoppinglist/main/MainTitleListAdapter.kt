package com.baddevelopergames.shoppinglist.main

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.baddevelopergames.shoppinglist.R
import com.baddevelopergames.shoppinglist.db.ListModel
import com.baddevelopergames.shoppinglist.utils.AvatarPicker
import kotlinx.android.synthetic.main.row_title_list.view.*

class MainTitleListAdapter constructor(private val list: List<ListModel>,
                                       private val avatarPicker: AvatarPicker,
                                       private val windowManager: WindowManager,
                                       private val interactions: MainTitleListAdapterInteractions)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var context: Context? = null

    companion object {
        const val VIEW_TYPE_EMPTY = 1
        const val VIEW_TYPE_NORMAL = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        return if (viewType == VIEW_TYPE_EMPTY) {
            val view = LayoutInflater.from(context)
                    .inflate(R.layout.row_empty_title_list, parent, false)
            EmptyTitleListViewHolder(view)
        } else {
            val view = LayoutInflater.from(context)
                    .inflate(R.layout.row_title_list, parent, false)
            TitleListViewHolder(view)
        }
    }

    override fun getItemCount(): Int {
        return if (list.isEmpty()) {
            1
        } else {
            list.size
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (list.isEmpty()) {
            VIEW_TYPE_EMPTY
        } else {
            VIEW_TYPE_NORMAL
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is TitleListViewHolder) {
            val listModel = list[position]
            holder.bind(listModel, avatarPicker, windowManager, list.size, interactions)
        }
    }

    class TitleListViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(listModel: ListModel, avatarPicker: AvatarPicker, windowManager: WindowManager,
                 listSize: Int, interactions: MainTitleListAdapterInteractions) {
            itemView.content.setBackgroundColor(Color.parseColor(listModel.color))
            itemView.content.setOnClickListener { interactions.onTitleClick(adapterPosition) }
            itemView.list_avatar.setBackgroundResource(avatarPicker.get(listModel.avatar))
            itemView.list_name.text = listModel.name
            if (listModel.remote) {
                itemView.remote_list_indicator.visibility = View.VISIBLE
            } else {
                itemView.remote_list_indicator.visibility = View.GONE
            }
            resize(windowManager, listSize)
        }

        private fun resize(windowManager: WindowManager, listSize: Int) {
            val width = getWidth(windowManager)
            val layoutParams = ViewGroup.LayoutParams(itemView.content.layoutParams)
            val divider = getDivider(listSize)
            layoutParams.width = ((width / divider).toInt())
            itemView.content.layoutParams = layoutParams
        }

        private fun getWidth(windowManager: WindowManager): Int {
            val display = windowManager.defaultDisplay
            val displayMetrics = DisplayMetrics()
            display.getMetrics(displayMetrics)
            return displayMetrics.widthPixels
        }

        private fun getDivider(listSize: Int): Float {
            return if (listSize <= 4) {
                listSize.toFloat()
            } else {
                4.5f
            }
        }
    }

    class EmptyTitleListViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface MainTitleListAdapterInteractions {
        fun onTitleClick(position: Int)
    }
}
