package com.baddevelopergames.shoppinglist.db

import com.google.firebase.auth.FirebaseAuth

class UserInfo {
    fun name(): String {
        val user = FirebaseAuth.getInstance().currentUser
        val fullName = user?.displayName ?: ""
        return extract(fullName)
    }

    private fun extract(fullName: String) = fullName.split(" ")[0]
}
