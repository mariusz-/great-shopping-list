package com.baddevelopergames.shoppinglist.db

import com.google.firebase.database.IgnoreExtraProperties
import java.util.*

@IgnoreExtraProperties
class ListModel : BaseFirebaseModel() {
    var color: String? = null
    var avatar: String? = null
    var content: HashMap<String, GroceryModel> = HashMap()
    var email: String? = null
    var remote = false
}
