package com.baddevelopergames.shoppinglist.db

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class GroceryModel : BaseFirebaseModel() {
    var url: String? = null
    var shop: Int = 0
}
