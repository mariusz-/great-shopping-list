package com.baddevelopergames.shoppinglist.db

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import io.reactivex.Observable

class DbExecutor {
    companion object {
        private const val TABLE_LISTS = "lists"
        private const val TABLE_CONTENT = "content"
        private const val TABLE_SHARED = "shared"
        private const val TABLE_SHARED_TO = "sharedTo"
    }

    fun getList(): Observable<ArrayList<ListModel>> {
        return getLocalList()
                .flatMap { list ->
                    getSharedListMetadata()
                            .flatMap { it ->
                                val obsList = ArrayList<Observable<ArrayList<ListModel>>>()
                                for (item in it) {
                                    obsList.add(getRemoteList(item.second, item.first))
                                }
                                if (obsList.size > 0) {
                                    Observable.zip(obsList) { results ->
                                        for (result in results) {
                                            list.addAll(result as Collection<ListModel>)
                                        }
                                        return@zip list
                                    }
                                } else {
                                    Observable.just(list)
                                }
                            }
                }
    }

    private fun getSharedListMetadata(): Observable<List<Pair<String, String>>> {
        return Observable.create {
            val dbRef = getDatabaseReference()
                    .child(TABLE_SHARED)
            dbRef.addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    it.onError(Throwable(p0.message))
                    dbRef.removeEventListener(this)
                }

                override fun onDataChange(p0: DataSnapshot) {
                    val resultList = ArrayList<Pair<String, String>>()
                    if (p0.value != null) {
                        resultList.addAll((p0.value as Map<String, String>).toList())
                    }
                    it.onNext(resultList)
                    it.onComplete()
                    dbRef.removeEventListener(this)
                }
            })
        }
    }

    private fun getRemoteList(user: String, listId: String): Observable<ArrayList<ListModel>> {
        return Observable.create {
            getUserDatabaseReference(user)
                    .child(TABLE_LISTS)
                    .child(listId)
                    .addValueEventListener(object : ValueEventListener {
                        override fun onCancelled(p0: DatabaseError) {
                            // fixme after logout this line cause crash
                            // it.onError(Throwable(p0.message))
                        }

                        override fun onDataChange(p0: DataSnapshot) {
                            val fetchedList = ArrayList<ListModel>()
                            val item = p0.getValue(ListModel::class.java)
                            if (item != null) {
                                item.remote = true
                                fetchedList.add(item)
                            }
                            it.onNext(fetchedList)
                            it.onComplete()
                        }
                    })
        }
    }

    fun getLocalList(): Observable<ArrayList<ListModel>> {
        return Observable.create {
            getTableDatabaseReference().addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    // fixme after logout this line cause crash
                    // it.onError(Throwable(p0.message))
                }

                override fun onDataChange(p0: DataSnapshot) {
                    val fetchedList = ArrayList<ListModel>()

                    for (dataSnapshot in p0.children) {
                        val item = dataSnapshot.getValue(ListModel::class.java)!!
                        fetchedList.add(item)
                    }
                    it.onNext(fetchedList)
                    it.onComplete()
                }
            })
        }
    }

    fun addList(listModel: ListModel): Observable<Any> {
        listModel.email = getPersonalizedPath(getCurrentEmail())
        return Observable.create { emitter ->
            getTableDatabaseReference().child(listModel.uid).setValue(listModel)
                    .addOnSuccessListener {
                        emitter.onNext(0)
                        emitter.onComplete()
                    }.addOnFailureListener {
                        emitter.onError(it)
                    }
        }
    }

    fun removeList(listModel: ListModel): Observable<Any> {
        return Observable.create { emitter ->
            getTableDatabaseReference().child(listModel.uid)
                    .removeValue()
                    .addOnSuccessListener {
                        getDatabaseReference().child(TABLE_SHARED_TO)
                                .child(listModel.uid)
                                .addValueEventListener(object : ValueEventListener {
                                    override fun onCancelled(p0: DatabaseError) {
                                    }

                                    override fun onDataChange(p0: DataSnapshot) {
                                        val emails = ArrayList<String>()
                                        for (dataSnapshot in p0.children) {
                                            emails.add(dataSnapshot.key!!)
                                        }
                                        getDatabaseReference().child(TABLE_SHARED_TO)
                                                .child(listModel.uid)
                                                .removeValue()
                                        for (email in emails) {
                                            getUserDatabaseReference(email)
                                                    .child(TABLE_SHARED)
                                                    .child(listModel.uid)
                                                    .removeValue()
                                        }
                                        getDatabaseReference().child(TABLE_SHARED_TO)
                                                .child(listModel.uid)
                                                .removeEventListener(this)
                                    }
                                })
                        emitter.onNext(0)
                        emitter.onComplete()
                    }.addOnFailureListener {
                        emitter.onError(it)
                    }
        }
    }

    fun getGroceryList(listId: String, email: String): Observable<List<GroceryModel>> {
        return Observable.create {
            getUserDatabaseReference(email)
                    .child(TABLE_LISTS)
                    .child(listId)
                    .child(TABLE_CONTENT)
                    .addValueEventListener(object : ValueEventListener {
                        override fun onCancelled(p0: DatabaseError) {
                            // fixme after logout this line cause crash
                            // it.onError(Throwable(p0.message))
                        }

                        override fun onDataChange(p0: DataSnapshot) {
                            val groceryList = ArrayList<GroceryModel>()
                            for (dataSnapshot in p0.children) {
                                val groceryModel = dataSnapshot.getValue(GroceryModel::class.java)
                                groceryModel?.let { it1 -> groceryList.add(it1) }
                            }
                            it.onNext(groceryList)
                        }
                    })
        }
    }

    fun addGrocery(name: String, parentId: String, email: String, url: String?, shop: Int): Observable<Any> {
        val grocery = GroceryModel()
        grocery.name = name
        grocery.url = url
        grocery.shop = shop
        return refreshGrocery(grocery, parentId, email)
    }

    fun refreshGrocery(groceryModel: GroceryModel, parentId: String, email: String): Observable<Any> {
        return Observable.create { emitter ->
            getUserDatabaseReference(email)
                    .child(TABLE_LISTS)
                    .child(parentId)
                    .child(TABLE_CONTENT)
                    .child(groceryModel.uid)
                    .setValue(groceryModel)
                    .addOnSuccessListener {
                        emitter.onNext(0)
                        emitter.onComplete()
                    }.addOnFailureListener {
                        emitter.onError(it)
                    }
        }
    }

    fun removeGrocery(groceryId: String, parentId: String, email: String): Observable<Any> {
        return Observable.create { emitter ->
            getUserDatabaseReference(email)
                    .child(TABLE_LISTS)
                    .child(parentId)
                    .child(TABLE_CONTENT)
                    .child(groceryId)
                    .removeValue()
                    .addOnSuccessListener {
                        emitter.onNext(0)
                        emitter.onComplete()
                    }.addOnFailureListener {
                        emitter.onError(it)
                    }
        }
    }

    private fun getUserDatabaseReference(user: String): DatabaseReference {
        val firebaseDatabase = FirebaseDatabase.getInstance()
        val databaseReference = firebaseDatabase.reference

        return databaseReference.child(getPersonalizedPath(user))
    }

    private fun getDatabaseReference(): DatabaseReference {
        return getUserDatabaseReference(getPersonalizedPath(getCurrentEmail()))
    }

    private fun getTableDatabaseReference(): DatabaseReference {
        return getDatabaseReference().child(TABLE_LISTS)
    }

    private fun getPersonalizedPath(user: String) = user.replace(".", "-")

    fun shareList(email: String, listId: String): Observable<Any> {
        return addSharedTo(email.trim(), listId)
                .flatMap {
                    return@flatMap addShare(email.trim(), listId)
                }
    }

    private fun getCurrentEmail() = FirebaseAuth.getInstance().currentUser!!.email!!

    private fun addSharedTo(email: String, listId: String): Observable<Any> {
        return Observable.create { emitter ->
            getDatabaseReference().child(TABLE_SHARED_TO)
                    .child(listId)
                    .child(getPersonalizedPath(email))
                    .setValue(getPersonalizedPath(email))
                    .addOnSuccessListener {
                        emitter.onNext(0)
                        emitter.onComplete()
                    }.addOnFailureListener {
                        emitter.onError(it)
                    }
        }
    }

    private fun addShare(email: String, listId: String): Observable<Any> {
        return Observable.create { emitter ->
            getUserDatabaseReference(email).child(TABLE_SHARED)
                    .child(listId)
                    .setValue(getPersonalizedPath(getCurrentEmail()))
                    .addOnSuccessListener {
                        emitter.onNext(0)
                        emitter.onComplete()
                    }.addOnFailureListener {
                        emitter.onError(it)
                    }
        }
    }

    fun getSharedTo(listId: String): Observable<ArrayList<String>> {
        return Observable.create { emitter ->
            getDatabaseReference().child(TABLE_SHARED_TO)
                    .child(listId)
                    .addValueEventListener(object : ValueEventListener {
                        override fun onCancelled(p0: DatabaseError) {
                            emitter.onError(p0.toException())
                        }

                        override fun onDataChange(p0: DataSnapshot) {
                            val sharedToList = ArrayList<String>()

                            for (dataSnapshot in p0.children) {
                                sharedToList.add(dataSnapshot.key as String)
                            }
                            emitter.onNext(sharedToList)
                            emitter.onComplete()
                        }
                    })
        }
    }

    fun removeSharedTo(listId: String, email: String) {
        getDatabaseReference().child(TABLE_SHARED_TO)
                .child(listId)
                .child(email)
                .removeValue()
        getUserDatabaseReference(email).child(TABLE_SHARED)
                .child(listId)
                .removeValue()
    }
}
