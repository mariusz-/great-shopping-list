package com.baddevelopergames.shoppinglist.db

import com.google.firebase.database.IgnoreExtraProperties
import java.io.Serializable
import java.util.*

@IgnoreExtraProperties
open class BaseFirebaseModel : Serializable {
    val uid = Calendar.getInstance().timeInMillis.toString()
    var name: String? = null
}
