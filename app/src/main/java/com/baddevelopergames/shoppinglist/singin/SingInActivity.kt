package com.baddevelopergames.shoppinglist.singin

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.baddevelopergames.shoppinglist.R
import com.baddevelopergames.shoppinglist.db.UserInfo
import com.baddevelopergames.shoppinglist.main.MainActivity
import kotlinx.android.synthetic.main.activity_sing_in.*

class SingInActivity : AppCompatActivity(), SingInActivityView {
    private val presenter by lazy {
        val userInfo = UserInfo()
        SingInActivityPresenter(this, userInfo)
    }

    companion object {
        private const val TAG_FORCE_SIGN_IN = "TAG_FORCE_SIGN_IN"

        fun callingIntent(context: Context, forceLogIn: Boolean): Intent {
            val intent = Intent(context, SingInActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            intent.putExtra(TAG_FORCE_SIGN_IN, forceLogIn)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sing_in)

        val forceSignIn = intent.getBooleanExtra(TAG_FORCE_SIGN_IN, false)
        presenter.singIn(forceSignIn)

        retry_btn.setOnClickListener { presenter.singIn(false) }
    }

    override fun singIn(intent: Intent, requestCode: Int) {
        startActivityForResult(intent, requestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        presenter.onActivityResult(requestCode, resultCode, data)
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun openMainActivity() {
        startActivity(MainActivity.callingIntent(this))
        finish()
    }

    override fun getContext(): Context {
        return this
    }
}
