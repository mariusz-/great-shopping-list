package com.baddevelopergames.shoppinglist.singin

import android.app.Activity
import android.content.Intent
import android.text.TextUtils
import com.baddevelopergames.shoppinglist.db.UserInfo
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import java.util.*

class SingInActivityPresenter constructor(private val view: SingInActivityView,
                                          private val userInfo: UserInfo) {
    companion object {
        const val RC_SING_IN = 123
    }

    fun singIn(forceSignIn: Boolean) {
        if (!forceSignIn && isLoggedIn()) {
            view.openMainActivity()
        } else {
            view.singIn(singInIntent(), RC_SING_IN)
        }
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RC_SING_IN) {
            val idpResponse = IdpResponse.fromResultIntent(data)

            if (resultCode == Activity.RESULT_OK) {
                view.showMessage("Hello, " + userInfo.name())
                view.openMainActivity()
            } else {
                view.showMessage("Sign in failed: " + idpResponse?.error?.message)
            }
        }
    }

    private fun isLoggedIn(): Boolean {
        return !TextUtils.isEmpty(userInfo.name())
    }

    private fun singInIntent(): Intent {
        return AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(providers())
                .build()
    }

    private fun providers(): List<AuthUI.IdpConfig> {
        return Arrays.asList(
                AuthUI.IdpConfig.GoogleBuilder().build()
        )
    }
}
