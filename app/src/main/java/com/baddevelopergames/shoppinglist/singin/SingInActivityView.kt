package com.baddevelopergames.shoppinglist.singin

import android.content.Context
import android.content.Intent

interface SingInActivityView {
    fun singIn(intent: Intent, requestCode: Int)

    fun showMessage(message: String)

    fun openMainActivity()

    fun getContext(): Context
}