package com.baddevelopergames.shoppinglist.deeplink

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.baddevelopergames.shoppinglist.R
import com.baddevelopergames.shoppinglist.main.MainActivity

class DeeplinkActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deeplink)

        val text = intent.getStringExtra(Intent.EXTRA_TEXT)
        startActivity(MainActivity.callingIntent(this, text))
        finish()
    }
}