package com.baddevelopergames.shoppinglist.base

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class RxPresenter {
    private var disposable = CompositeDisposable()

    fun onDestroy() {
        disposable.clear()
    }

    fun rxRequest(method: Disposable) {
        disposable.add(method)
    }
}