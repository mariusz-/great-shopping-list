package com.baddevelopergames.shoppinglist.base

import android.support.v7.app.AppCompatActivity

abstract class RxActivity : AppCompatActivity() {
    abstract val presenter: RxPresenter

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }
}