package com.baddevelopergames.shoppinglist.base

import android.support.v4.app.Fragment

abstract class RxFragment : Fragment() {
    abstract val presenter: RxPresenter

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.onDestroy()
    }
}