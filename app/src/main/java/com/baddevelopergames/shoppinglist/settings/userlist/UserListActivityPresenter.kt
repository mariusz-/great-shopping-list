package com.baddevelopergames.shoppinglist.settings.userlist

import android.content.Context
import com.baddevelopergames.shoppinglist.R
import com.baddevelopergames.shoppinglist.base.RxPresenter
import com.baddevelopergames.shoppinglist.db.DbExecutor
import com.baddevelopergames.shoppinglist.db.ListModel
import com.baddevelopergames.shoppinglist.settings.share.ShareDialogFactory
import com.baddevelopergames.shoppinglist.settings.share.ShareDialogFactory.Interactions
import com.baddevelopergames.shoppinglist.utils.sortAlphabetically
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuthRecentLoginRequiredException

class UserListActivityPresenter(private val view: UserListActivityView,
                                private val dbExecutor: DbExecutor,
                                private val shareDialogFactory: ShareDialogFactory) : RxPresenter() {
    private val list = ArrayList<ListModel>()

    private val shareDialogInteractions: ShareDialogFactory.Interactions = object : Interactions {
        override fun onShared(email: String, listId: String) {
            rxRequest(
                    dbExecutor.shareList(email, listId)
                            .subscribe(
                                    {
                                        view.showMessage(R.string.list_shared)
                                    },
                                    {
                                        view.showMessage(it.message)
                                    }))
        }
    }

    fun init() {
        view.initRecyclerView(list)
    }

    fun fetchList() {
        rxRequest(
                dbExecutor.getLocalList()
                        .subscribe(
                                {
                                    list.clear()
                                    list.addAll(it.sortAlphabetically())
                                    view.notifyDataSetChanged()
                                },
                                {
                                    view.showMessage(it.message)
                                }
                        ))
    }

    fun addListSelected() {
        view.openAddListActivity()
    }

    fun removeList(listModel: ListModel) {
        rxRequest(dbExecutor.removeList(listModel)
                .subscribe(
                        {
                            list.remove(listModel)
                            view.notifyDataSetChanged()
                        },
                        {
                            view.showMessage(it.message)
                        }
                ))
    }

    fun logout(context: Context) {
        AuthUI.getInstance()
                .delete(context)
                .addOnSuccessListener {
                    view.openSignInActivity(false)
                }.addOnFailureListener {
                    if (it is FirebaseAuthRecentLoginRequiredException) {
                        view.openSignInActivity(true)
                    } else {
                        view.showMessage(it.message)
                    }
                }
    }

    fun changeList(listModel: ListModel) {
        view.openAddListActivity(listModel)
    }

    fun shareList(context: Context, listModel: ListModel) {
        rxRequest(
                dbExecutor.getSharedTo(listModel.uid)
                        .subscribe({
                            shareDialogFactory.create(context, shareDialogInteractions, listModel.uid, it,
                                    dbExecutor::removeSharedTo)
                        }, {
                            view.showMessage(it.message)
                        }))
    }
}
