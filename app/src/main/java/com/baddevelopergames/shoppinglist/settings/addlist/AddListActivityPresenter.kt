package com.baddevelopergames.shoppinglist.settings.addlist

import android.graphics.Color
import com.baddevelopergames.shoppinglist.R
import com.baddevelopergames.shoppinglist.base.RxPresenter
import com.baddevelopergames.shoppinglist.db.DbExecutor
import com.baddevelopergames.shoppinglist.db.ListModel
import com.baddevelopergames.shoppinglist.utils.AvatarPicker
import com.baddevelopergames.shoppinglist.utils.ColorPicker

class AddListActivityPresenter constructor(val view: AddListActivityView,
                                           private val dbExecutor: DbExecutor,
                                           private val colorPicker: ColorPicker,
                                           private val avatarPicker: AvatarPicker) : RxPresenter() {
    fun addList() {
        refreshList(ListModel())
    }

    fun refreshList(listModel: ListModel) {
        view.enableBtn(false)
        val inputName = view.inputName()
        if (inputName.isNotEmpty()) {
            val inputColor = view.inputColor()
            val inputAvatar = view.inputAvatar()
            listModel.avatar = inputAvatar
            listModel.color = inputColor
            listModel.name = inputName.trim()
            rxRequest(
                    dbExecutor.addList(listModel).subscribe(
                            {
                                view.close()
                            },
                            {
                                view.showMessage(it.message)
                                view.enableBtn(true)
                            }))
        } else {
            view.enableBtn(true)
            view.showMessage(R.string.name_cannot_be_empty)
        }
    }

    fun pickColor() {
        val inputColor = view.inputColor()
        rxRequest(
                colorPicker.pick(Color.parseColor(inputColor))
                        .subscribe {
                            view.refreshColor(it, colorPicker.name(it))
                        })
    }

    fun pickAvatar() {
        rxRequest(
                avatarPicker.pick()
                        .subscribe {
                            refreshAvatar(it)
                        })
    }

    fun applyUpdateContent(name: String?, color: String?, avatar: String?) {
        view.refreshName(name)
        view.refreshColor(Color.parseColor(color), color)
        refreshAvatar(avatar)
        view.changeBtnLabel(R.string.update_list)
    }

    private fun refreshAvatar(avatar: String?) {
        val resId = avatarPicker.get(avatar)
        view.refreshAvatar(resId, avatar)
    }
}
