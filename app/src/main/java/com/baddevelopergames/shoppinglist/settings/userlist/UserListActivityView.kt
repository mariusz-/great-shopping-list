package com.baddevelopergames.shoppinglist.settings.userlist

import com.baddevelopergames.shoppinglist.db.ListModel

interface UserListActivityView {
    fun showMessage(message: String?)

    fun showMessage(message: Int)

    fun initRecyclerView(list: List<ListModel>)

    fun openAddListActivity()

    fun openAddListActivity(listModel: ListModel)

    fun notifyDataSetChanged()

    fun openSignInActivity(forceSignIn: Boolean)
}