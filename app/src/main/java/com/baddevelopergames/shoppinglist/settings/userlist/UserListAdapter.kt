package com.baddevelopergames.shoppinglist.settings.userlist

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.baddevelopergames.shoppinglist.R
import com.baddevelopergames.shoppinglist.db.ListModel
import com.baddevelopergames.shoppinglist.utils.AvatarPicker
import kotlinx.android.synthetic.main.row_create_list.view.*

class UserListAdapter constructor(private val list: List<ListModel>,
                                  private val avatarPicker: AvatarPicker,
                                  private val interactions: CreateListAdapterInteractions) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        const val VIEW_TYPE_EMPTY = 1
        const val VIEW_TYPE_NORMAL = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_TYPE_EMPTY) {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_create_list_empty, parent, false)
            EmptyCreateListViewHolder(view)
        } else {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_create_list, parent, false)
            CreateListViewHolder(view)
        }
    }

    override fun getItemCount(): Int {
        return if (list.isEmpty()) {
            1
        } else {
            list.size
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (list.isEmpty()) {
            VIEW_TYPE_EMPTY
        } else {
            VIEW_TYPE_NORMAL
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CreateListViewHolder) {
            val listModel = list[position]
            holder.bind(listModel, avatarPicker, interactions)
        } else if (holder is EmptyCreateListViewHolder) {
            holder.bind(interactions)
        }
    }

    class CreateListViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(listModel: ListModel, avatarPicker: AvatarPicker,
                 interactions: CreateListAdapterInteractions) {
            itemView.list_avatar.setBackgroundResource(avatarPicker.get(listModel.avatar))
            itemView.list_name.text = listModel.name
            itemView.content.setBackgroundColor(Color.parseColor(listModel.color))
            itemView.content.setOnClickListener { interactions.changeList(listModel) }
            itemView.content.setOnLongClickListener {
                interactions.removeList(listModel)
                true
            }
            itemView.share.setOnClickListener { interactions.shareList(listModel) }
        }
    }

    class EmptyCreateListViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val addList = itemView.findViewById<View>(R.id.add_list)

        fun bind(interactions: CreateListAdapterInteractions) {
            addList.setOnClickListener { interactions.addList() }
        }
    }

    interface CreateListAdapterInteractions {
        fun addList()

        fun removeList(listModel: ListModel)

        fun changeList(listModel: ListModel)

        fun shareList(listModel: ListModel)
    }
}
