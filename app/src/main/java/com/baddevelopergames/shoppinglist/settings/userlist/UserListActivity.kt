package com.baddevelopergames.shoppinglist.settings.userlist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.baddevelopergames.shoppinglist.R
import com.baddevelopergames.shoppinglist.base.RxActivity
import com.baddevelopergames.shoppinglist.db.DbExecutor
import com.baddevelopergames.shoppinglist.db.ListModel
import com.baddevelopergames.shoppinglist.settings.addlist.AddListActivity
import com.baddevelopergames.shoppinglist.settings.share.ShareDialogFactory
import com.baddevelopergames.shoppinglist.singin.SingInActivity
import com.baddevelopergames.shoppinglist.utils.AvatarPicker
import kotlinx.android.synthetic.main.activity_user_list.*

class UserListActivity : RxActivity(), UserListActivityView,
        UserListAdapter.CreateListAdapterInteractions {
    private var adapter: UserListAdapter? = null
    override val presenter: UserListActivityPresenter by lazy {
        val dbExecutor = DbExecutor()
        val shareDialogFactory = ShareDialogFactory()
        UserListActivityPresenter(this, dbExecutor, shareDialogFactory)
    }

    companion object {
        fun callingIntent(context: Context): Intent {
            return Intent(context, UserListActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_list)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        presenter.init()
    }

    override fun onResume() {
        super.onResume()
        presenter.fetchList()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_user_list, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> finish()
            R.id.add_list -> presenter.addListSelected()
            R.id.logout -> presenter.logout(this)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun initRecyclerView(list: List<ListModel>) {
        val lm = LinearLayoutManager(this)
        list_container.layoutManager = lm
        val avatarPicker = AvatarPicker(this)
        adapter = UserListAdapter(list, avatarPicker, this)
        list_container.setHasFixedSize(true)
        list_container.adapter = adapter
    }

    override fun notifyDataSetChanged() {
        adapter?.notifyDataSetChanged()
    }

    override fun showMessage(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showMessage(message: Int) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun openAddListActivity() {
        startActivity(AddListActivity.callingIntent(this))
    }

    override fun addList() {
        openAddListActivity()
    }

    override fun removeList(listModel: ListModel) {
        presenter.removeList(listModel)
    }

    override fun openSignInActivity(forceSignIn: Boolean) {
        startActivity(SingInActivity.callingIntent(this, forceSignIn))
    }

    override fun changeList(listModel: ListModel) {
        presenter.changeList(listModel)
    }

    override fun openAddListActivity(listModel: ListModel) {
        startActivity(AddListActivity.callingIntent(this, listModel))
    }

    override fun shareList(listModel: ListModel) {
        presenter.shareList(this, listModel)
    }
}
