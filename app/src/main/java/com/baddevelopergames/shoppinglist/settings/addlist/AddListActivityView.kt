package com.baddevelopergames.shoppinglist.settings.addlist

interface AddListActivityView {
    fun inputName(): String

    fun showMessage(message: Int)

    fun showMessage(message: String?)

    fun inputColor(): String

    fun inputAvatar(): String

    fun close()

    fun refreshColor(color: Int, name: String?)

    fun refreshAvatar(resId: Int, name: String?)

    fun refreshName(name: String?)

    fun enableBtn(enable: Boolean)

    fun changeBtnLabel(resId: Int)
}