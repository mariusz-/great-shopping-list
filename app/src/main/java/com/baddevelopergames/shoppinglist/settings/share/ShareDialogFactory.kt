package com.baddevelopergames.shoppinglist.settings.share

import android.app.AlertDialog
import android.content.Context
import android.support.design.chip.Chip
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.Toast
import com.baddevelopergames.shoppinglist.R

class ShareDialogFactory {
    fun create(context: Context, interactions: Interactions, listId: String,
               sharedTo: ArrayList<String>, c: (String, String) -> Unit) {
        val customView = LayoutInflater.from(context).inflate(R.layout.dialog_share_list, null)
        val shareEmailEt = customView.findViewById<EditText>(R.id.share_email_et)
        val sharedAlreadyContainer = customView.findViewById<ScrollView>(R.id.shared_already_container)
        val sharedAlreadyLl = customView.findViewById<LinearLayout>(R.id.shared_already_ll)
        if (sharedTo.isNotEmpty()) {
            sharedAlreadyContainer.visibility = View.VISIBLE
            for (item in sharedTo) {
                val chip = LayoutInflater.from(context).inflate(R.layout.item_shared, null) as Chip
                chip.chipText = item
                chip.setOnCloseIconClickListener {
                    c.invoke(listId, item)
                    sharedAlreadyLl.removeView(chip)
                }
                sharedAlreadyLl.addView(chip)
            }
        }
        AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.share_list))
                .setView(customView)
                .setNegativeButton(context.getString(R.string.cancel)) { dialogInterface, _ ->
                    dialogInterface.dismiss()
                }
                .setPositiveButton(context.getString(R.string.share)) { dialogInterface, _ ->
                    val input = shareEmailEt.text.toString()
                    if (TextUtils.isEmpty(input)) {
                        Toast.makeText(context, context.getString(R.string.email_cannot_be_empty),
                                Toast.LENGTH_SHORT).show()
                    } else {
                        interactions.onShared(input, listId)
                        dialogInterface.dismiss()
                    }
                }
                .create()
                .show()
    }

    interface Interactions {
        fun onShared(email: String, listId: String)
    }
}
