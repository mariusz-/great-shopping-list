package com.baddevelopergames.shoppinglist.settings.addlist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import com.baddevelopergames.shoppinglist.R
import com.baddevelopergames.shoppinglist.base.RxActivity
import com.baddevelopergames.shoppinglist.db.DbExecutor
import com.baddevelopergames.shoppinglist.db.ListModel
import com.baddevelopergames.shoppinglist.utils.AvatarPicker
import com.baddevelopergames.shoppinglist.utils.ColorPicker
import kotlinx.android.synthetic.main.activity_add_list.*

class AddListActivity : RxActivity(), AddListActivityView {
    override val presenter: AddListActivityPresenter by lazy {
        val dbExecutor = DbExecutor()
        val colorPicker = ColorPicker(this)
        val avatarPicker = AvatarPicker(this)
        AddListActivityPresenter(this, dbExecutor, colorPicker, avatarPicker)
    }

    companion object {
        private const val TAG_LIST = "TAG_LIST"

        fun callingIntent(context: Context): Intent {
            return Intent(context, AddListActivity::class.java)
        }

        fun callingIntent(context: Context, list: ListModel): Intent {
            val intent = Intent(context, AddListActivity::class.java)
            intent.putExtra(TAG_LIST, list)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_list)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        input_color.setOnClickListener { presenter.pickColor() }
        input_avatar.setOnClickListener { presenter.pickAvatar() }

        val extras = intent.extras
        if (extras != null) {
            val list = extras.get(TAG_LIST) as ListModel
            presenter.applyUpdateContent(list.name, list.color, list.avatar)
            add_list.setOnClickListener { presenter.refreshList(list) }
        } else {
            add_list.setOnClickListener { presenter.addList() }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            close()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun inputName(): String {
        return input_list_name.text.toString()
    }

    override fun showMessage(message: Int) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun showMessage(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun close() {
        finish()
    }

    override fun inputColor(): String {
        return input_color_name.text.toString()
    }

    override fun inputAvatar(): String {
        return input_avatar_name.text.toString()
    }

    override fun refreshColor(color: Int, name: String?) {
        input_color_name.text = name
        input_color.setBackgroundColor(color)
    }

    override fun refreshAvatar(resId: Int, name: String?) {
        input_avatar_name.text = name
        input_avatar.setImageResource(resId)
    }

    override fun enableBtn(enable: Boolean) {
        add_list.isEnabled = enable
    }

    override fun refreshName(name: String?) {
        input_list_name.setText(name)
    }

    override fun changeBtnLabel(resId: Int) {
        add_list.text = getString(resId)
    }
}
